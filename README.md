# Welcome to the Institute for Practicing Aryans
Click [here](https://instituteforpracticingaryans.gitlab.io/ipa/index.html) to get started!

## Disclaimer
If you are viewing this repository on a shared host (GitHub/GitLab) some functionality will be limited, for the best experience we recommend cloning it to your desktop. Future releases are subject to change, library files will be inevitably excluded from this directory, and there will be a user interface provided for desktop users at a later date. However there is no plan to provide a user interface for mobile users, so they will have to contend with their device's browser. If you are new to the Institute, please know all binaries are password encrypted and their titles do not necessary reflect the content found within.

# Backup Mirror
Mirror 1: [DigitalTelePresence](https://git.digitaltelepresence.com/instituteforpracticingaryans/ipa)
